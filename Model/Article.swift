//
//  Article.swift
//  iso-premleapheng-homework-005
//
//  Created by JONNY on 9/18/1399 AP.
//

import Foundation
import SwiftyJSON


struct Article {
    let id: String
    let description: String
    
    init(json: JSON) {
        self.id = json["_id"].stringValue
        self.description = json["description"].stringValue
        
    }
}
