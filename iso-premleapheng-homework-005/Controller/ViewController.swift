//
//  ViewController.swift
//  iso-premleapheng-homework-005
//
//  Created by JONNY on 9/18/1399 AP.
//

import UIKit
import Alamofire
import SwiftyJSON
		
protocol SendDataDelegate {
    func data(id:String)
}

class ViewController: UIViewController {


    @IBOutlet weak var tableView: UITableView!
    
    var delegateObj: SendDataDelegate?
    var article = [Article]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.delegate = self
        tableView.dataSource = self

        let url = "http://110.74.194.124:3000/api/articles"
        
        AF.request(url, method: .get).responseJSON { (respon) in
           switch respon.result{
           case .success(let data): let json = JSON(data)
                for (_, subJson):(String, JSON) in json["data"] {
                    self.article.append(Article(json: subJson))
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .failure(_):
                print("Fetch Failed")
            }
        }
    }


}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.article.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = article[indexPath.row].id
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailView") as! DetailViewController
        delegateObj = vc 
        delegateObj?.data(id: article[indexPath.row].id)
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        // Pass the indexPath as the identifier for the menu configuration
        let open = UIAction(title: "Open", image: UIImage(systemName: "doc.on.doc")) { _ in

            print("open")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailView") as! DetailViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        let delete = UIAction(title: "Delete", image: UIImage(systemName: 	"bin.xmark.fill")) { _ in
            
            let urlDelete = "http://110.74.194.124:3000/api/articles/\(self.article[indexPath.row].id)"
            AF.request(urlDelete, method: .delete).response{ response in
                print(response)
            }
            
            self.article.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            
        }
        
        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { _ in
            // Empty menu for demonstration purposes
            return UIMenu(title: "", children: [open,delete])
        }
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {

        // Delete Data from API
        let urlDelete = "http://110.74.194.124:3000/api/articles/\(self.article[indexPath.row].id)"
        AF.request(urlDelete, method: .delete).response{ response in
            print(response)
        }
        
        article.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
            
        }
    }
}

