//
//  DetailViewController.swift
//  iso-premleapheng-homework-005
//
//  Created by JONNY on 9/18/1399 AP.
//

import UIKit
import Alamofire
import SwiftyJSON

class DetailViewController: UIViewController, SendDataDelegate {

    var getID : String = ""

    @IBOutlet weak var textView: UITextView!
    let url = "http://110.74.194.124:3000/api/articles"
    override func viewDidLoad() {
        super.viewDidLoad()
        print(getID)
        
        
        AF.request("http://110.74.194.124:3000/api/articles/\(getID)").responseData { response in
            switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let articleTemp = Article(json: json["data"])
                        
                    self.textView.text = articleTemp.description

                    case .failure(_):
                        print("Fetch Failed")
            }
        }
    }
   
    
    func data(id: String) {
        getID = id
    }
    
    
    @IBAction func deleteItem(_ sender: Any) {
        let alert = UIAlertController(title: "Delete Article", message: "Do you want to delete this article?∫®®", preferredStyle: .alert)

        
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.destructive, handler: { _ in
            let urlDelete = "http://110.74.194.124:3000/api/articles/\(self.getID)"
            AF.request(urlDelete, method: .delete).response{ response in
                print(response)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true)
        
    }
}
